package com.nakul.springbootdemo.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nakul.springbootdemo.resources.Fruit;
import com.nakul.springbootdemo.server.DemoController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class ServiceCentral {

    private final String success = "SUCCESS";

    private ArrayList<Fruit> fruitList = new ArrayList<>();

    @Autowired
    private ObjectMapper objectMapper;

    public String parse(String string) {
        try {
            Fruit fruit = objectMapper.readValue(string, Fruit.class);
            fruitList.add(fruit);
        } catch (Exception e) {
            return "FAILURE: " + e;
        }
        return "SUCCESS";
    }

    public String printList() {
        String returnString = "";
        for (int i = 0; i < fruitList.size(); i++) {
            returnString = returnString + fruitList.get(0).toString() + "\n";
        }
        return returnString;
    }

}
