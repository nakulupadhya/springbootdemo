package com.nakul.springbootdemo.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nakul.springbootdemo.service.ServiceCentral;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DemoConfiguration {

    @Bean
    public ServiceCentral serviceCentral(ObjectMapper objectMapper) {
        return new ServiceCentral();
    }

}
