package com.nakul.springbootdemo.server;

import com.nakul.springbootdemo.resources.Fruit;
import com.nakul.springbootdemo.service.ServiceCentral;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping(path = "/")
@RestController
@ComponentScan(basePackages = {"com.nakul.springbootdemo.*"})
public class DemoController {

    private final ServiceCentral serviceCentral;

    @Autowired
    public DemoController(ServiceCentral serviceCentral) {
        this.serviceCentral = serviceCentral;
    }


    @PostMapping(path = "health")
    public String healthCheck() {
        return "Service is up";
    }

    @PostMapping(path = "echo")
    public String echo(@RequestBody String string) {
        return string + " " + string + " " + string;
    }

    @PostMapping(path = "parse")
    public String parse(@RequestBody String string){
        return serviceCentral.parse(string);
    }

    @PostMapping(path = "print")
    public String print(){
        return serviceCentral.printList();
    }



}
