package com.nakul.springbootdemo.resources;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Fruit {
    @JsonProperty("type")
    private String type;
    @JsonProperty("color")
    private String color;
    @JsonProperty("size")
    private String size;

    private static String success = "success";

    public String toString() {
        return String.format("Fruit: %s. Color: %s, Size: %s", type, color, size);
    }

}
